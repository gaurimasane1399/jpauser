package com.accenture.testing;

import static org.junit.Assert.assertEquals;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import com.accenture.entity.UserEntity;

public class SelectTest {
	
	EntityManagerFactory factory = null;
	EntityManager em = null;
	
	@BeforeAll
	public void setup() {
		factory = Persistence.createEntityManagerFactory("unit1");
		em = factory.createEntityManager();
	}
	
	@Test
	public void selectTest() {
		UserEntity u = em.find(UserEntity.class, 102);

		assertEquals("user1", u.getUserName());
		assertEquals("user123", u.getUserPass());
	}
	


	@AfterAll
	public void close() {
		em.close();
		factory.close();
	}
}
