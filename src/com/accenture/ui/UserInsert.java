package com.accenture.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.entity.UserEntity;

public class UserInsert {

	public static void main(String[] args) {

	

		EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
		
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		
		UserEntity user1 = new UserEntity(101, "User1", "User111");
		UserEntity user2 = new UserEntity(102, "User2", "User112");
		UserEntity user3 = new UserEntity(103, "User3", "User113");
		UserEntity user4 = new UserEntity(104, "User4", "User114");
		
		em.persist(user1);
		em.persist(user2);
		em.persist(user3);
		em.persist(user4);
		
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

}
