package com.accenture.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.entity.UserEntity;

public class UserDelete {

	public static void main(String[] args) {

EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
		
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		
		UserEntity user = em.find(UserEntity.class, 101);
		if(user!=null) {
			em.remove(user);
		}
		
		
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

}
