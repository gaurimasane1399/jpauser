package com.accenture.ui;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import com.accenture.entity.UserEntity;

public class UserUpdate {

	public static void main(String[] args) {

EntityManagerFactory factory = Persistence.createEntityManagerFactory("unit1");
		
		EntityManager em = factory.createEntityManager();
		
		em.getTransaction().begin();
		
		UserEntity user1 = new UserEntity(105,"User5","User124");
		
		
		UserEntity user = em.find(UserEntity.class, 102);
		if(user!=null) {
			user.setUserName("UserName1");
			user.setUserPass("user");
		}
		em.merge(user1);
		em.getTransaction().commit();
		em.close();
		factory.close();
	}

}
